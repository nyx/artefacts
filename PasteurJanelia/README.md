`pipeline_extract_states.tgz` is MatLab source copied from */pasteur/zeus/projets/p02/hecatonchire/pipeline_larva_all/pipeline_states_maestro* (Institut Pasteur, on July 21, 2024) with main function *pipeline_states_maestro* renamed *pipeline_extract_states* as in [https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia/tree/master/pipeline_extract_states](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia/tree/master/pipeline_extract_states).

The code in the `pipeline_extract_states.tgz` file has been updated by JBM (and to a lesser extent by FL) since pushed on GitHub. The code has never been version-controlled nor released under a specific license.

The latest version requires a fifth “layer” of classifiers that cannot be found on GitHub. The corresponding classifier parameter files are available here as `Classifier_fifth_layer_RF_compact.tgz`.

The `pipeline_extract_states_2023b` is an executable file compiled with mcc from `pipeline_extract_states.tgz` modified for [PasteurJanelia-adapter](https://gitlab.pasteur.fr/nyx/PasteurJanelia-adapter).
