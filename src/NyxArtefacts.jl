module NyxArtefacts

using Tar, Inflate, SHA

export print_sha

function print_sha(file)
    println("sha256: ", bytes2hex(open(sha256, file)))
    println("git-tree-sha1: ", Tar.tree_hash(IOBuffer(inflate_gzip(file))))
end

end # module
